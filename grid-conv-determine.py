
#############################################################################
##                                                                         ##
##   Latest update: 09/Apr/2023                                            ##
##                                                                         ##
##   script by    _ _              _ _                                     ##
##               | |_|_           (_) |_                                   ##
##    _ __   __ _|  _|_|_ _ _    _ _|  _|                                  ##
##   | '_  \/ _' | | |/ _' | |/\| | | |                                    ##
##   | | | | (_| | | | (_| |  /\  | | |  _                                 ##
##   |_| |_|\__'_|_|_|\__'_|_/  \_|_|_| (_)                                ##
##                                                                         ##   
##   Comment                                                               ##
##   Script to determine grid convergence from known geodetic coordinate   ##
##                                                                         ##
#############################################################################

# import modules
from math import pi
import numpy
import utm


def utm_to_latlon(easting, northing, zone_number, zone_letter):

    #   Comments
    #       Python module from github: https://github.com/Turbo87/utm
    #       UTM to Latitude/Longitude convert an UTM coordinate into a (latitude, longitude) tuple.
    #       Example:
    #           >>> utm.to_latlon(340000, 5710000, 32, 'U')
    #           >>> (51.51852098408468, 6.693872395145327)

    #       The syntax is utm.to_latlon(EASTING, NORTHING, ZONE_NUMBER, ZONE_LETTER).
    #       The return has the form (LATITUDE, LONGITUDE).

    utm_to_latlon_parameter = utm.to_latlon(easting, northing, zone_number, zone_letter)
    
    return utm_to_latlon_parameter


def convergence_on_the_projection(lat, lon, lon0):
    
    #   Comments
    #       The convergence angle (conv) at a point on the projection is defined by
    #       the angle measured from the projected meridian, which defines true north,
    #       to a grid line, defining grid north.
    #       
    #       The convergence must be added to a grid bearing to obtain a bearing from true north.
    #       For the secant transverse Mercator the convergence may be expressed as:
    #
    #       conv = arctan(tan(lon-lon0) * sin(lat))
    #       where,
    #       lon  = longitude
    #       lon0 = central meridian of the oberserver's UTM zone 
    #       lat  = lattitude

    convergence = numpy.arctan(numpy.tan((lon - lon0) * pi/180) * numpy.sin(lat * pi/180))
    convergence = convergence * 180/pi

    return convergence


# The UTM coordinate of POINT 'O' at VGOS site
# REMARK: The UTM coordinate of POINT 'O' acquires from field surveying. 
EASTING  = 522951.637
NORTHING = 2085974.466

# The UTM zone of POINT 'O' at VGOS site
# WARNING: Please be careful for entering zone number, zone letter, central meridian.
#          Since, the zone depends on the location of observer.
ZONE_NUMBER = 47
ZONE_LETTER = 'Q'
CENTRAL_MERIDIAN = 99.00000

utm_to_latlon_tuple = utm_to_latlon(EASTING, NORTHING, ZONE_NUMBER, ZONE_LETTER)

# Lat/Lon coordinate of POINT 'O'
lat = utm_to_latlon_tuple[0]
lon = utm_to_latlon_tuple[1]

# Determine the convergence angle
convergence = convergence_on_the_projection(lat, lon, CENTRAL_MERIDIAN)

print('[DEBUG] east: {}, north: {} [Unit: m]'.format(EASTING, NORTHING))
print('[DEBUG] lat: {}, lon: {} [Unit: deg]'.format(lat, lon))
print('[DEBUG] conv: {} [Unit: deg]'.format(convergence))
