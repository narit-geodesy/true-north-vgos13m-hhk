## True North determination by conventional surveying at VGOS telescope 13m construction site, Chiang Mai



## Overview

True North (the direction of the earth's rotational axis) at VGOS telescope 13m construction site, Chiang Mai (in short, VGOS site), can be determined through Grid North that obtained from 2 control survey points (GPS06 and GPS11) at TNRO site. Grid north is normally base on the Universal Transverse Mercator (UTM) coordinate system. To determine True North, it is necessary to make a grid correction by adding meridian convergence, which symmetrically depends on the UTM zone, Latitude and Longitude of the observer. Therefore, this project aims to describe a solution for accurate True North determination.

Among TNRO site, there are many monuments built from concrete with deep foundation setting to make them stand still on the ground. Some monument can be used for survey control points. In this project, we prefer to use GPS06 and GPS11 for survey control points.

| Point | Grid Northing (m) | Grid Easting (m) |         Note         |
|:-----:|:-----------------:|:----------------:|:---------------------|
| GPS06 | 2085852.475	    | 522856.689	   | WGS84 / UTM zone 47Q |
| GPS11 | 2085783.994	    | 522892.029	   | WGS84 / UTM zone 47Q |

## Grid North

As we know, the 2 control points (GPS06 and GPS11) have their own UTM coordination. Thus, we can make surveying with <ins>Closed Loop Traversing Technique</ins> from these control points to VGOS site.

## True North
We make a correction using Gauss-Bomford convention.

## Grid North and True North relation
![](./image/Grid-north-and-true-north-relation.jpg/)

https://www.drillingformulas.com/magnetic-declination-and-grid-convergent-and-their-applications-in-directional-drilling/



## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/narit-geodesy/true-north-vgos13m-hhk.git
git branch -M main
git push -uf origin main
```

## License

Open project
